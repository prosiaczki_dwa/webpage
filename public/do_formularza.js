
function pobierz_pole_z_formularza(formularz, pole) {
    let firstname = formularz.elements[pole];
    if (!firstname){
        throw "Formularz nie ma pola " + pole;
    }

    let name = firstname.value;
    if (!name){
        throw "Prosze podaj jakies imie."
    }
    return name;
}

function wypisz_dane_z_formularza() {
    var oForm = document.getElementById("do_wypisania");
    try {
        var name = pobierz_pole_z_formularza(oForm, "firstname");
        document.getElementById("tekst").innerHTML = name + " uwielbia prosiaczki!";
        document.getElementById("tekst_error").innerHTML = "";
    } catch (aaa) {
        document.getElementById("tekst").innerHTML = "";
        document.getElementById("tekst_error").innerHTML = aaa;
    }
}

