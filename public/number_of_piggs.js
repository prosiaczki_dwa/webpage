function how_many_piggs() {

    var inpObj = document.getElementById("number_of_piggs");
    if (!inpObj.checkValidity()) {
        var number = parseInt(inpObj.value);
        if (isNaN(number)) {
            document.getElementById("number_of_piggs_alert").innerHTML = "Wpisz wartosc liczbowa";
        }
        else if (number == 1) {
            document.getElementById("number_of_piggs_alert").innerHTML = "Samotna swinka jest smutna...";
        }
        else {
            document.getElementById("number_of_piggs_alert").innerHTML = "Zla wartosc!!";
        }
    } else {
        document.getElementById("number_of_piggs_alert").innerHTML = "Dobrze! Swinki zyja w grupie!"
    }
}