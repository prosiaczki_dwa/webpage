function fill_date() {
    document.getElementById('data').innerHTML = Date();
    document.getElementById('month').innerHTML = "";
}

function fill_month() {
    var date_str = document.getElementById('data').innerHTML;
    var myRegexp = /[a-z]+ ([a-z]+) .*/i;
    var match = myRegexp.exec(date_str);
    try {
        var month = match[1];
        document.getElementById('month').innerHTML = month;
    } catch (error) {
        document.getElementById('month').innerHTML = "Kliknij przycisk obok!";
    }
}