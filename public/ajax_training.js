function loadDocWithAjax() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        // line with status checks commented out to make it work on chrome
        // if (this.readyState == 4 && this.status == 200) {
        if (this.readyState == 4) {
            document.getElementById("ajax_training_div").innerHTML =
                this.responseText;
        }
    };
    xhttp.open("GET", "ukryta_wiadomosc.txt", true);
    xhttp.send();
}

function loadDocFromXml() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        // line with status checks commented out to make it work on chrome
        // if (this.readyState == 4 && this.status == 200) {
        if (this.readyState == 4) {
            fillUpPiggyNames(this);
        }
    };
    xhttp.open("GET", "piggy_names.xml", true);
    xhttp.send();
}

function fillUpPiggyNames(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var table="<tr><th>Piggy</th><th>Name</th></tr>";
    var x = xmlDoc.getElementsByTagName("SWINKA");
    for (i = 0; i <x.length; i++) {
      table += "<tr><td>" +
      x[i].getElementsByTagName("PIGGY")[0].childNodes[0].nodeValue +
      "</td><td>" +
      x[i].getElementsByTagName("BREED")[0].childNodes[0].nodeValue +
      "</td></tr>";
    }
    document.getElementById("ajax_table").innerHTML = table;
  }