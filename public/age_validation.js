function validate_age() {

    var x, text;
    x = parseInt(document.getElementById("age").value);
    if (isNaN(x) || x < 0 || x > 122) {
        text = "Wrong input";
    } else {
        text = "Jest OK! :-D";
    }
    document.getElementById("validate_age_text").innerHTML = text;
}