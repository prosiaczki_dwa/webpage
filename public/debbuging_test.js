

a = 5;
b = 6;
c = a + b;
console.log(c);

var add1 = function () {
    a = a + 1;
    console.log("add_1")
}
console.log(add1)

var add2 = (function () {
    a = a + 1;
    console.log("add_2")
})();
console.log(add2)

var add3 = function () {
    var innerAdd = function () {
        a = a + 1;
        console.log("add_3");
    };
    return innerAdd
}
console.log(add3)

var add4 = (function () {
    var innerAdd = function () {
        a = a + 1;
        console.log("add_4");
    };
    return innerAdd
})();
console.log(add4)
// add1()